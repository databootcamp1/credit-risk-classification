# credit-risk-classification
Creator: Melissa Acevedo

## Overview of the Analysis

The purpose of this analysis to develop machine learning models to predict loan default risks based on financial information provided in the dataset. The purpose was to assist financial institutions in making informed decisions about loan approvals and risk management.

The dataset contains financial information such as loan size, interest rate, borrower income, debt-to-income ratio, number of accounts, derogatory marks, total debt, and loan status. We sought to predict the loan status, which represents whether a loan is healthy (0) or high-risk (1).

The stages of the machine learning process included:

Data preprocessing: Standardization and splitting the data into training and testing sets.
Model training: Using logistic regression to build the predictive models.
Model evaluation: Assessing model performance using accuracy, precision, and recall scores.

## Results

Logistic Regression:

Training Set:
* Accuracy: 99%
* Precision (Class 1): 86%
* Recall (Class 1): 90%

Testing Set:
* Accuracy: 99%
* Precision (Class 1): 85%
* Recall (Class 1): 91%

## Summary

The logistic regression model performed well in predicting loan default risks, achieving high accuracy, precision, and recall scores on both the training and testing sets. It consistently identified high-risk loans while minimizing false positives and false negatives.

Considering the robust performance and interpretability of logistic regression, I recommend deploying this model for use by the company. Its ability to accurately classify loans and provide insights into the factors contributing to default risk makes it a valuable tool for decision-making in loan approvals and risk management.